<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //הכרחי לשמירת הזמנים

class MeetingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meetings')->insert([
            [
            'date' => '2020.10.08',
            'hour' => '18:00',
            'created_at' => Carbon::now(), //סיפריית קרבון מאפשרת להכניס את הזמן של עכשיו
            'updated_at' => Carbon::now()
            ],
            [
            'date' => '2020.10.12',
            'hour' => '14:00',
            'created_at' => Carbon::now(), //סיפריית קרבון מאפשרת להכניס את הזמן של עכשיו
            'updated_at' => Carbon::now()
            ],
            [
            'date' => '2020.10.14',
            'hour' => '10:00',
            'created_at' => Carbon::now(), //סיפריית קרבון מאפשרת להכניס את הזמן של עכשיו
            'updated_at' => Carbon::now()
            ],

        ]);  
    }
}
