<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            ['status_id' => 1, 'status_name' => "Treatment has not started yet", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 2, 'status_name' => "Further treatment is required", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 3, 'status_name' => "Treatment ended", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 4, 'status_name' => "A future appointment has been set", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],

        ]);
    }
}
