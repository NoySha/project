<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //הכרחי לשמירת הזמנים


class HmoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('hmos')->insert([
        [
            'name' => "Clalit",
            'location' => "Shmuel Hanavi 6",
            'created_at' => Carbon::now(),
            'updated_at'=>Carbon::now()
        ],
        [   'name' => "Macabi",
            'location' => "Tiltan 11",
            'created_at' => Carbon::now(),
            'updated_at'=>Carbon::now()
        ],
        [   'name' => "Mehuhedet",
            'location' => "Mesilat Yosef 24",
            'created_at' => Carbon::now(),
             'updated_at'=>Carbon::now()
        ],
        [   'name' => "Lehumit",
            'location' => "Rabi Akiva 43",
            'created_at' => Carbon::now(),
            'updated_at'=>Carbon::now()
        ],
        ]);

    }
}
