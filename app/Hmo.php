<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Hmo extends Model
{

    protected $primaryKey = 'HMO_id'; 

    public function clients(){ 
        return $this -> hasMany('App\Client', 'client_id'); 
    }
}
