<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\DB;
use App\Meeting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clients = Client::select(DB::raw("COUNT(*) as count"))
                  ->whereYear('created_at',date('Y'))
                  ->groupBy(DB::raw("Month(created_at)"))
                  ->pluck('count');
        $months = Client::select(DB::raw("Month(created_at) as month"))
                  ->whereYear('created_at',date('Y'))
                  ->groupBy(DB::raw("Month(created_at)"))
                  ->pluck('month');
        
        $datas = array(0,0,0,0,0,0,0,0,0,0,0,0);
        foreach($months as $index => $month)
        {
            $datas[$month] = $clients[$index];
        }
        return view('home',compact('datas','clients','month'));
    }

    public function barChart(){

        $clients = Client::select(DB::raw("COUNT(*) as count"))
                  ->whereYear('created_at',date('Y'))
                  ->groupBy(DB::raw("Month(created_at)"))
                  ->pluck('count');
        $months = Client::select(DB::raw("Month(created_at) as month"))
                  ->whereYear('created_at',date('Y'))
                  ->groupBy(DB::raw("Month(created_at)"))
                  ->pluck('month');
        
                  $datas = array(0,0,0,0,0,0,0,0,0,0,0,0);
                  foreach($months as $index => $month){
            $datas[$month-1] = $clients[$index];
        }

        return view('bar-chart',compact('datas','clients','months'));

    }


}
