<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Available;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\File;
use Carbon\Carbon;
use Illuminate\Support\Str;


class AvailablesController extends Controller
{
    
    public function index()
    {
        Gate::authorize('Meetings');
        $users = User::all();
        return view('secretary.available',compact('users'));
    }

    public function showhours(Request $request)
    {
        Gate::authorize('Meetings');
        $date = $request->get('date');
        $user_id = $request->get('user_id'); 
        if ($date==null){
            Session::flash('date', 'You must select date');
            return back();
        }
        if ($user_id==null){
            Session::flash('user', 'You must select psychologist');
            return back();
        }
        if ($date==null){
            Session::flash('dateuser', 'You must select date and/or psychologist');
            return back();
        }
        $hours = ['08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00'];
        $availables = Available::findHoursDelete($date,$user_id);
        $user = User::findOrFail($user_id);

        return view('secretary.available2',compact('hours','user','date','availables'));
    }

    public function store(Request $request)
    {        
        $avail = new Available(); 

        $ava = $avail->create($request->all());
        $ava->status="available";
        $ava->save();

        return back();
    }

    
    public function indexdelete()
    {
        Gate::authorize('Meetings');
        $users = User::all();
        
        return view('secretary.indexdelete',compact('users'));
    }

    public function deletehours(Request $request)
    {
        Gate::authorize('Meetings');
        $date = $request->get('date');
        $user_id = $request->get('user_id'); 
        $user = User::findOrFail($user_id);
        if ($date==null){
            Session::flash('date', 'You must select date');
            return back();
        }
        if ($user==null){
            Session::flash('user', 'You must select psychologist');
            return back();
        }
        if ($date==null){
            Session::flash('dateuser', 'You must select date and/or psychologist');
            return back();
        }
        $availables = Available::findHoursDelete($date,$user_id);
        if($availables==null){
            Session::flash('noHours', 'No Available Schedule');
            return back();
        }
        return view('secretary.deleteavailable',compact('availables','user','date'));
    }




    public function deleteh(Request $request,$id)
    {
        $available=Available::findOrFail($id); 
        $available->delete();
        return back();
    }


    public function importUser()
    {
        return view('secretary.import-available');
    }
 
    public function handleImportUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
 
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator);
        }
 
        $file = $request->file('file');
        $csvData = file_get_contents($file);
        $rows = array_map("str_getcsv", explode("\n", $csvData));
        $header = array_shift($rows);
 
        $extension = $request->file('file')->getClientOriginalExtension();
        $validextensions = array("txt");        
        if(in_array(strtolower($extension), $validextensions)){

        foreach ($rows as $row) {
            if ($row!=[null] && (count($row)==count($header))){
                $row = array_combine($header, $row);
    
                Available::create([
                    'user_id' => $row['id'],
                    'date' => $row['date'],
                    'start_hour' => $row['start_hour'],
                    'status' => 'available',
                ]);
            }
        }
        Session::flash('upload','The data was uploaded successfully');
        return redirect()->back();
    }
        else{
            Session::flash('notupload','please select .txt file');
            return redirect()->back();
                }
}




}
