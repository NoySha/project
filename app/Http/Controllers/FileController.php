<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Illuminate\Support\Facades\Storage;
use App\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $files=File::fileClient($id);
        $client=Client::findOrFail($id);

        return view('file.index',compact('files','client'));
    }

    public function clientFile($id)
    {
        $files=File::fileClient($id);
        $client=Client::findOrFail($id);
        return view('file.index',compact('files','client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        Gate::authorize('ShowMyClients');
        $client=Client::findOrFail($id);
        $types=["Referral","Medicines","Other"];
        return view('file.upload',compact('client','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        Gate::authorize('ShowMyClients');
        $this->validate($request,[
        'file'=>'required|file|max:20000'
        ]);
        $upload=$request->file('file');
        $path=$upload->store('pubblic/storage');
        $file=File::create([
            'title' => $request->type,
            'description'=>$id,
            'path'=> $path
        ]);
        $files=File::fileClient($id);
        $client=Client::findOrFail($id);
        Session::flash('success', 'file upload');
        return view('file.index',compact('files','client'))->with('success','file upload');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dl=File::find($id);
        return Storage::download($dl->path,$dl->title);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('ShowMyClients');
        $del = File::find($id);
        Storage::delete($del->path);
        $del->delete();
        return back();
    }
}
