<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Client;
use App\Hmo;
use App\Meeting;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Http\Controllers\IntlChar;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index()
    {
        $meetings = Meeting::all();
        $users = User::all();
        $roles = Role::all();
        $clients = Client::paginate(5);
        $hmos = Hmo::all();
        $statuses = Status::all();
        return view('clients.index',compact('meetings','users','hmos','clients','statuses'));
    }

    public function todo()
    {
        $meetings = Meeting::all();
        $users = User::all();
        $roles = Role::all();
        $clients = Client::todo1();
        $hmos = Hmo::all();
        $statuses = Status::all();
        return view('clients.todo',compact('meetings','users','hmos','clients','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $hmos = Hmo::all();
        $statuses = Status::all();

        return view('clients.create',compact('users','hmos','statuses'));
    }

    public function search1()
    {
        $users = User::all();
        $hmos = Hmo::all();
        $statuses = Status::all();
        $client = Client::all();
        return view('clients.select',compact('users','hmos','statuses','client'));
    }

    public function search(Request $request)
    {
        $client_id = $request->get('search');
        $client = Client::find($client_id);
        $meetings = Meeting::all();
        $users = User::all();
        $roles = Role::all();
        $hmos = Hmo::all();
        $statuses = Status::all();
        if (Client::find($client_id)!=null){
        return view('clients.show',compact('users','hmos','client','statuses','meetings'));
        }
        else{
        Session::flash('notfound', 'id not exist');
        return back();

        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('Clients');
        $client_id = $request->get('id'); 
        $clientExist = Client::exist($client_id);

        $email = $request->get('email'); 
        $clientExistmail = Client::existmail($email);
        if($clientExistmail=='1'){
            Session::flash('existmailclient', 'This mail already exist');
            return back();
        }
        elseif($clientExist=='1'){
            Session::flash('exist', 'This id already exist');
            return back();
        }
        
        else{
        $client = new Client(); //אובייקט ריק שיכיל את פרטי הלקוח החדש
                   $client = $client->create($request->all()); //שימוש בקיאייט בגלל המס אסיימנט
                   $client->status_id = 1;
                   $client->save(); //שמירת הנתונים של הטופס
                   return view('clients.show',compact('client'));       }
        
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id); 
        return view('clients.show',compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Gate::authorize('Clients');

        $client = Client::findOrFail($id); // 
        $users = User::all();
        $hmos = Hmo::all();
        return view('clients.edit', compact('client','hmos','users')); //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('Clients');
        $email = $request->get('email'); 
        $clientExistmail = Client::existmail($email);
        if($clientExistmail=='1'){
            Session::flash('existmailclient', 'This mail already exist');
            return back();
        }
        else{
        $client = Client::findOrFail($id); 
        $client->update($request->all()); 
        return redirect('clients');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showMyClients(){
        Gate::authorize('ShowMyClients');
        $user = Auth::user();
        $clients = Client::all();
        return view('clients.MyClients',compact('user','clients'));
    }


    public function changeStatus($cid, $sid){ 
        Gate::authorize('ShowMyClients');
        $client=Client::findOrFail($cid); 
        $client->status_id = $sid; 
        $client->save(); 
        return back(); 
    }
    

    public function medicalHistory($id)
    {
        Gate::authorize('ShowMyClients');
        $client = Client::findOrFail($id); 
        $user = Auth::user();// 
        $statuses = Status::all();

        return view('clients.medicalHistory', compact('client','user','statuses')); //
    }

    public function complete($cid){
        $client = Client::findOrFail($cid);
        $client->status_id = 4;
        $client->save();
        return back();
    }


}
