<?php
 
 namespace App\Http\Controllers\Auth;

 use App\Http\Controllers\Controller;
 use App\Providers\RouteServiceProvider;
 use App\User;
 use App\Role;
 use Illuminate\Foundation\Auth\RegistersUsers;
 use Illuminate\Support\Facades\Hash;
 use Illuminate\Support\Facades\Validator;
 
 use Illuminate\Http\Request;
 use Illuminate\Support\Str;
 use Illuminate\Support\Facades\Auth;
 use Illuminate\Support\Facades\Gate;
 use Illuminate\Support\Facades\Session;
 use Illuminate\Http\RedirectResponse;
 
class RegisterController extends Controller
{
 /*
 |--------------------------------------------------------------------------
 | Register Controller
 |--------------------------------------------------------------------------
 |
 | This controller handles the registration of new users as well as their
 | validation and creation. By default this controller uses a trait to
 | provide this functionality without requiring any additional code.
 |
 */
 
 //need to overide the function showRegistrationForm() if this trait
 use RegistersUsers;
 
 //override show registration form 
 //from the RegistersUsers trait 

 
 public function showRegistrationForm()
 {
 $roles = Role::all();
 return view('auth.register', compact('roles'));
 }
 
 /**
 * Where to redirect users after registration.
 *
 * @var string
 */
 //protected $redirectTo = RouteServiceProvider::HOME;
 
 /**
 * Create a new controller instance.
 *
 * @return void
 */
 /*
 public function __construct()
 {
 $this->middleware('guest');
 }
 */
 /**
 * Get a validator for an incoming registration request.
 *
 * @param array $data
 * @return \Illuminate\Contracts\Validation\Validator
 */
 
protected function validator(array $data)
{
return Validator::make($data, [
'id' => ['required', 'integer', 'min:9', 'max:9', 'confirmed', 'unique:users'],
'name' => ['required', 'string', 'max:255'],
'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
'password' => ['required', 'string', 'min:8', 'confirmed'],
]);
}
 
 /**hhhhhhh
 * Create a new user instance after a valid registration.
 *
 * @param array $data
 * @return \App\User
 */
 
protected function createuser(request $request)
{

    $user_id = $request->get('id'); 
    $userExist = User::exist($user_id);
    if($userExist=='1'){
        Session::flash('exist', 'This id already exist');
        return back();
    }

    $email = $request->get('email'); 
    $userExistmail = User::existmail($email);
    if($userExistmail=='1'){
        Session::flash('existmail', 'This mail already exist');
        return back();
        
    }
    $password = ($request['password']);
    $passwordconfirm = ($request['password-confirm']);
    if($password!=$passwordconfirm){
        return back()->withErrors(['password-confirm' => ['The passwords do not match']]);

    }

    User::create([
        'id' => $request['id'],
        'name' => $request['name'],
        'email' => $request['email'],
        'password' => Hash::make($request['password']),
        'role_id' => $request['role_id'],
    ]);
    return redirect('users');
}

protected function create(array $data)
 {
 return User::create([
 'id' => $data['id'],
 'name' => $data['name'],
 'email' => $data['email'],
 'password' => Hash::make($data['password']),
 'role_id' => $data['role_id'],
 ]);
 }
 
}