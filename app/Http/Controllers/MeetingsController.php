<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Client;
use App\Hmo;
use App\Meeting;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Hour;
use App\Available;
use Carbon\Carbon;


//use Barryvdh\DomPDF\PDF;
use PDF;

class MeetingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $roles = Role::all();
        $clients = Client::all();
        $meetings = Meeting::paginate(15);

        return view('meetings.index',compact('meetings','users','clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cid)
    {
        Gate::authorize('Meetings');
        $users = User::all();
        $client = Client::find($cid);
        $user=$client->user_id;
        $avails=Available::avail($user);
        return view('meetings.create',compact('users','client','avails'));
    }

    public function createshow(Request $request)
    {
        $cid = $request->id;
        $users = User::all();
        $client = Client::find($cid);
        //return view('meetings.create',compact('users','client'));
        echo $client;
    }

    public function search()
    {
        $users = User::all();
        return view('meetings.search',compact('users'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meeting = new Meeting(); //אובייקט ריק שיכיל את פרטי המפגש
        $meet = $meeting->create($request->all()); //שימוש בקיאייט בגלל המס אסיימנט
        $meet->save(); //שמירת הנתונים של הטופס
        $hour_id = $request->get('hour'); 
        $available = Available::findOrFail($hour_id);
        $available->status="occupied"; 
        $available->save();
        return redirect('meetings'); //העברת היוזר לטבלת הפגישות
    }

    public function findavail(Request $request)
    {
        Gate::authorize('Meetings');
        $user_id = $request->get('user_id'); 
        $date = $request->get('date'); 
        $users = User::all();
        $cid = $request->client_id;
        $client = Client::find($cid);
        $hours=Available::findHours($date,$user_id);
        if($hours==null){
            Session::flash('noavailable', 'No available meetings. Please choose other date');
            return back();
        }
        return view('meetings.create2',compact('users','client','date','user_id','hours'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //  
    }

    public function showselected(Request $request)
    {
    $user_id = $request->get('user_id'); 
    $date = $request->get('date');  
    //$user = User::findOrFail($user_id);
    $user_name=User::user_name($user_id);
    $clients = Client::all();
    if($date==null && $user_id==null){
            Session::flash('notselected', 'Please select date and/or psychologist');
            return back(); 
    
    }
    elseif ($user_id==null){
        $meetings = Meeting::date($date);
        if($meetings==null){
            Session::flash('nomeetingsdate', 'There are no meetings on this date');
            return back();
        }
    }
    elseif($date==null){
        $meetings = Meeting::myMeetings($user_id);
        if($meetings==null){
            Session::flash('nomeetingsuser', 'psychologist do not have meetings');
            return back();
        }
    }

    else{
        $meetings = Meeting::datepsy($user_id,$date);
        if($meetings==null){
            Session::flash('nomeeting', 'There are no meetings');
            return back();
    }
}
    return view('meetings.search2',compact('meetings','user_id','clients','date','user_name'));
    }

    public function bla(Request $request)
    {
    $user_id = $request->get('user_id'); 
    $date = $request->get('date');  
    $users = User::all();

    $clients = Client::all();

    if ($user_id==null){
        $meetings = Meeting::date($date);
        if($meetings==null){
            Session::flash('nomeetingsdate', 'There are no meetings on this date');
            return back();
        }
    }
    elseif($date==null){
        $meetings = Meeting::myMeetings($user_id);
        if($meetings==null){
            Session::flash('nomeetingsuser', 'psychologist do not have meetings');
            return back();
        }
    }

    else{
        $meetings = Meeting::datepsy($user_id,$date);
        if($meetings==null){
            Session::flash('nomeeting', 'There are no meetings');
            return back();
    }
}
    return view('meetings.indexclient',compact('meetings','users','clients','date','user_id'));
    }

    public function showclientmeet($client_id)
    {   
    $users = User::all();
    $clients = Client::all();
    $meetings = Meeting::client($client_id);
    if($meetings->isEmpty()){
        Session::flash('nomeetings', 'The client does not have meetings');
        return back();
    }
    return view('meetings.indexclient',compact('meetings','users','clients'));
    }

    public function showselectedpsy(Request $request)
    {
    $user_id = Auth::id(); 
    $user = User::findOrFail($user_id);
    $date = $request->get('date');  
    $clients = Client::all();
    if($date==null){
        Session::flash('notselectedpsy', 'Please select date');
        return back(); 
    }
    else{
        $meetings = Meeting::datepsy($user_id,$date);
        if($meetings==null){
            Session::flash('nomeetingsuser', 'psychologist do not have meetings');
            return back();
        }
    }
    return view('meetings.MyMeetings',compact('meetings','user','clients'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $meeting = Meeting::findOrFail($id); // 
       // return view('meetings.edit', compact('meeting')); //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meeting = Meeting::findOrFail($id); 
        $meeting->update($request->all()); 
        return redirect('meetings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meeting = Meeting::findOrFail($id);
        $meeting ->delete(); 
        $hour_id = $meeting->hour; 
        $available = Available::findOrFail($hour_id);
        $available->status="available"; 
        $available->save();
        return redirect('meetings');
    }

    public function changepsycho($id,$uid){
        $meeting=Meeting::findOrFail($id); //
        $meeting->user_id = $uid; // 
        $meeting->save(); //
        return back(); //
    }
    
    public function changeclient($id,$cid){
        $meeting=Meeting::findOrFail($id); //
        $meeting->client_id = $cid; // 
        $meeting->save(); //
        return back(); //
    }

    public function showMyMeetings(){
        Gate::authorize('ShowMyMeetings');
        $user = Auth::user();
        $user_id=$user->id;
        $meetings = Meeting::myMeetings($user_id);
        $clients = Client::all();
        return view('meetings.MyMeetings',compact('user','clients','meetings'));
    }

    public function addsummary(Request $request, $id)
    {

       $meeting = Meeting::findOrFail($id);
       $meeting->update($request->all());
        return back();
    }



    public function downloadPDF(Request $request){
 $user_id = $request->get('user_id'); 
    $date = $request->get('date');       
    $users = User::all();
    $clients = Client::all();
    if ($user_id==null){
        $meetings = Meeting::date($date);
        if($meetings==null){
            Session::flash('nomeetingsdate', 'There are no meetings on this date');
            return back();
        }
    }
    elseif($date==null){
        $meetings = Meeting::myMeetings($user_id);
        if($meetings==null){
            Session::flash('nomeetingsuser', 'psychologist do not have meetings');
            return back();
        }
    }

    else{
        $meetings = Meeting::datepsy($user_id,$date);
        if($meetings==null){
            Session::flash('nomeeting', 'There are no meetings');
            return back();
    }
}
        $pdf = PDF::loadView('meetings.downloadindex',compact('meetings','users','clients'));
        return $pdf->download('meet-list.pdf');
    }

}
//
