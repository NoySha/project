<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Meeting extends Model
{
    protected $fillable = ['date','hour','psychologist','client_id','user_id','summary'];//כל המשתנים שנאשר לערוך

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function clients2(){ 
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function available(){ 
        return $this->belongsTo('App\Available','hour');
    }


    public static function myMeetings($user_id){
        $meetings = DB::table('meetings')->where('user_id', $user_id)->pluck('id');
        return self::find($meetings)->all();
    }

    public static function medicalHistory($client_id){
        $meetings = DB::table('meetings')->where('client_id', $client_id)->pluck('id');
        return self::find($meetings)->all();
    }

    public static function datepsy($user_id,$date){
        $meetings = DB::table('meetings')->where('user_id', $user_id)->where('date',$date)->pluck('id');
        return self::find($meetings)->all();
    }

    public static function date($date){
        $meetings = DB::table('meetings')->where('date',$date)->pluck('id');
        return self::find($meetings)->all();
    }


    public static function client($client_id){
        $meetings = DB::table('meetings')->where('client_id', $client_id)->pluck('id');
        return self::find($meetings);
        ;
    }

}
