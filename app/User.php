<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    protected $fillable = [
        'id','name', 'email', 'password','role_id'
    ];


    public function meetings(){
        return $this->hasMany('App\Meeting','meeting_id');
    }

    public function clients3(){
        return $this->hasMany('App\Client','client_id');
    }

        
    public function roles(){
        return $this->belongsTo('App\Role','role_id');
    }
    
    public function isAdmin(){
        $role = $this->roles;
        if(!isset($role)) return false;
   
            if($role->name === 'admin') return true; 
  
        return false; 
    }

    public function isSecretary(){
        $role = $this->roles;
        if(!isset($role)) return false;
   
            if($role->name === 'secretary') return true; 
  
        return false; 
    }

    public function isPsychologis(){
        $role = $this->roles;
        if(!isset($role)) return false;
   
            if($role->name === 'psychologist') return true; 
  
        return false; 
    }

    public function isSecretaryoradmin(){
        $role = $this->roles;
        if(!isset($role)) return false;
   
            if($role->name === 'admin' || $role->name === 'secretary') return true; 
  
        return false; 
    }

    public function isPsychologisoradmin(){
        $role = $this->roles;
        if(!isset($role)) return false;
   
            if($role->name === 'psychologist' || $role->name === 'secretary') return true; 
  
        return false; 
    }
    public static function exist($user_id){
        $users = DB::table('users')->where('id',$user_id)->pluck('id');
        if(!$users->isEmpty()) return '1';
        return '0';
        }

        public static function  existmail($email){
            $users = DB::table('users')->where('email',$email)->pluck('id');
            if(!$users->isEmpty()) return '1';
            return '0';
            }

       

        public static function user_name($user_id){
            $name = DB::table('users')->where('id', $user_id)->pluck('id');
            return self::find($name)->pluck('name');
        }  


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
