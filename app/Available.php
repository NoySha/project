<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Available extends Model
{
    protected $fillable = [
        'date', 'end_hour','start_hour', 'user_id','status'
    ];

    public function meetings(){ 
        return $this->belongsTo('App\Meeting','id');
    }


    public static function findHours($date,$user_id){
        $hours = DB::table('availables')->where('date', $date)->where('user_id',$user_id)->where('status','available')->pluck('id');
        return self::find($hours)->sortBy('start_hour');
    }

    public static function addHours($hour,$date,$user){
        $d = DB::table('availables')->where('start_hour',$hour)->where('user_id',$user)->where('date',$date)->pluck('id');
        if(!$d->isEmpty()) return '1';
        return '0';
        }

        public static function findHoursDelete($date,$user_id){
            $avail = DB::table('availables')->where('date', $date)->where('user_id',$user_id)->pluck('id');
            return self::find($avail)->all();
        } 

        public static function avail($user){
            $avails = DB::table('availables')->where('status', 'available')->where('user_id', $user)->pluck('id');
            return self::find($avails)->pluck('date');
        }   
}