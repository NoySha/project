<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    protected $fillable = [
        'name', 'email', 'phone','HMO_id','status_id','Psychologist','user_id','id'
    ];
    
    public function hmos(){ 
    return $this->belongsTo('App\Hmo','HMO_id'); 
    }


    public function user3(){ 
    return $this->belongsTo('App\User','user_id'); 
    }

    public function meetings2(){
        return $this->hasMany('App\Meeting','meeting_id');
    }

    public static function myClients($user_id){
        $clients = DB::table('clients')->where('user_id', $user_id)->pluck('id');
        return self::find($clients)->all();
 
    }

    public function status(){ 
        return $this->belongsTo('App\Status','status_id'); 
    }

    public static function todo1(){
        $todo = DB::table('clients')->where('status_id', 2)->pluck('id');
        return self::find($todo)->all();
 
    }

    public static function exist($client_id){
        $clients = DB::table('clients')->where('id',$client_id)->pluck('id');
        if(!$clients->isEmpty()) return '1';
        return '0';
        }

    public static function  existmail($email){
        $client = DB::table('clients')->where('email',$email)->pluck('id');
        if(!$client->isEmpty()) return '1';
        return '0';
        }   
    
}
