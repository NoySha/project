<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model
{
    protected $fillable = [
        'title', 'path','description'
    ];



    public static function fileClient($id){
        $fileClient = DB::table('files')->where('description', $id)->pluck('id');
        return self::find($fileClient)->all();
    }
}


