<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('edit-user', function ($user) {
            return $user->isAdmin();
        });  

        

        Gate::define('add-user', function ($user) {
            return $user->isAdmin();
        });  

 
        Gate::define('change-role', function ($user) {
            return $user->isAdmin();
        });  
    

        Gate::define('Meetings', function ($user) {
            return $user->isSecretary();
        }); 

        Gate::define('Clients', function ($user) {
            return $user->isSecretary();
        }); 
        
        
        
        Gate::define('ShowMyMeetings', function ($user) {
            return $user->isPsychologis();
            }); 

            Gate::define('ShowMyClients', function ($user) {
                return $user->isPsychologis();
                }); 

                
            Gate::define('ClientsList', function ($user) {
                return $user->isSecretaryoradmin();
                }); 

            Gate::define('Clients.mettings', function ($user) {
                return $user->isPsychologisoradmin();
                });     
}
}