<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//users routes
Route::resource('users', 'UsersController')->middleware('auth');
Route::get('users/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('auth');
Route::get('users/{id}/editpass', 'UsersController@editpass')->name('users.editpass')->middleware('auth');
Route::get('editselfpass', 'UsersController@editselfpass')->name('users.editselfpass')->middleware('auth');

Route::post('users/{id}/update/', 'UsersController@update')->name('users.update')->middleware('auth');
Route::post('users/{id}/updatepass/', 'UsersController@updatepass')->name('users.updatepass')->middleware('auth');
Route::get('users/changerole/{id}/{rid}', 'UsersController@changerole')->name('users.changerole')->middleware('auth');
Route::get('adduser', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('user.adduser')->middleware('auth');
Route::post('createuser', '\App\Http\Controllers\Auth\RegisterController@createuser')->name('user.createuser')->middleware('auth');


//meetings routes
Route::get('meetings', 'MeetingsController@index')->name('meetings.index')->middleware('auth');
//Route::get('meetings/create', 'MeetingsController@create')->name('meetings.create')->middleware('auth');
Route::post('meetings', 'MeetingsController@store')->name('meetings.store')->middleware('auth');
//Route::get('meetings/{id}/edit', 'MeetingsController@edit')->name('meetings.edit')->middleware('auth');
Route::post('meetings/{id}/update/', 'MeetingsController@update')->name('meetings.update')->middleware('auth');
Route::get('meetings/{id}/delete/', 'MeetingsController@destroy')->name('meetings.delete')->middleware('auth');
//Route::get('meetings/changepsychologist/{id}/{uid}', 'MeetingsController@changepsycho')->name('meetings.changepsycho')->middleware('auth');
Route::get('meetings/changeclient/{id}/{cid}', 'MeetingsController@changeclient')->name('meetings.changeclient')->middleware('auth');
Route::get('MyMeetings', 'MeetingsController@showMyMeetings')->name('Meetings.MyMeetings')->middleware('auth');
Route::get('medicalHistory/summary/{id}', 'MeetingsController@addsummary')->name('meetings.addsummary')->middleware('auth');
Route::get('selectedmeeting', 'MeetingsController@showselected')->name('meetings.showselected')->middleware('auth');
Route::get('clientmeet/{client_id}', 'MeetingsController@showclientmeet')->name('meetings.showclientmeet')->middleware('auth');
Route::get('meetings/create/{cid?}', 'MeetingsController@create')->name('meetings.create')->middleware('auth');
Route::get('selectedmeeting1', 'MeetingsController@bla')->name('meetings.showselected2')->middleware('auth');

Route::post('findavailable', 'MeetingsController@findavail')->name('meetings.find')->middleware('auth');

//available roues
Route::resource('available', 'AvailablesController')->middleware('auth');
Route::get('available2', 'AvailablesController@showhours')->name('hours')->middleware('auth');
Route::post('available', 'AvailablesController@store')->name('available.store')->middleware('auth');


Route::get('indexdelete', 'AvailablesController@indexdelete')->name('available.indexdelete')->middleware('auth');
Route::get('deleteavailablehours', 'AvailablesController@deletehours')->name('available.deletehours')->middleware('auth');
Route::get('deleteavailable/{id}', 'AvailablesController@deleteh')->name('available.deleteh')->middleware('auth');


Route::get('availableImport', 'AvailablesController@importUser')->name('import-user');
Route::post('availableImport', 'AvailablesController@handleImportUser')->name('bulk-import-user');


//clients routes
Route::resource('clients', 'ClientsController')->middleware('auth');
Route::get('clients', 'ClientsController@index')->name('clients.index')->middleware('auth');
Route::post('clients', 'ClientsController@store')->name('clients.store')->middleware('auth');
Route::get('clients/{id}/edit', 'ClientsController@edit')->name('clients.edit')->middleware('auth');
Route::post('clients/{id}/update/', 'ClientsController@update')->name('clients.update')->middleware('auth');
Route::get('clients/{id}/delete/', 'ClientsController@destroy')->name('clients.delete')->middleware('auth');
Route::get('clients/changepsychologist/{id}/{uid}', 'ClientsController@changepsycho')->name('clients.changepsycho')->middleware('auth');
//Route::get('clients/changeHmo/{id}/{hid}', 'ClientsController@changeHmo')->name('clients.changeHmo')->middleware('auth');
Route::get('MyClients', 'ClientsController@showMyClients')->name('Clients.MyClients')->middleware('auth');
Route::get('clients/changestatus/{cid}/{sid}','ClientsController@changeStatus')->name('clients.changestatus')->middleware('auth');
Route::get('clients/{id}/medicalHistory', 'ClientsController@medicalHistory')->name('clients.medicalHistory')->middleware('auth');
Route::get('todo', 'ClientsController@todo')->name('clients.todo')->middleware('auth');
Route::get('clients/complete/{cid?}', 'ClientsController@complete')->name('clients.complete')->middleware('auth');


//find client + add meeting
Route::get('search1', 'ClientsController@search1')->name('clients.search1')->middleware('auth');
Route::get('search', 'ClientsController@search')->name('clients.search')->middleware('auth');

//find meeting + download
Route::get('searchmeet', 'MeetingsController@search')->name('meetings.search')->middleware('auth');
Route::get('searchdownload', 'MeetingsController@searchdownload')->name('meetings.searchdownload')->middleware('auth');
Route::get('downloadPDF','MeetingsController@downloadPDF')->name('downloadPDF');
Route::get('searchmeetpsyc', 'MeetingsController@showselectedpsy')->name('meetings.showselectedpsy')->middleware('auth');



//file
Route::get('/file/{id}','FileController@index')->name('viewfile');

Route::get('/file/upload/{id}','FileController@create')->name('formfile');
Route::post('/file/upload/{id}','FileController@store')->name('uploadfile');
Route::get('/file/download/{id}','FileController@show')->name('downloadfile');
Route::get('/file/{id}/show','FileController@destroy')->name('deletefile');



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/bar-chart', 'HomeController@barChart')->name('barChart')->middleware('auth');
Route::get('/bar-chart2', 'HomeController@barChart2')->name('barChart2')->middleware('auth');