@extends('layouts.app')

@section('title', 'find client')
@section('page title', 'find client')

@section('content')

@if(Session::has('notfound'))
<div class = 'alert alert-danger'>
    {{Session::get('notfound')}}
</div>
@endif

 <div class="container">
 <div class="row justify-content-center">
 <div class="col-md-8">


 <div class="card">
              <div class="card-body">
              <div class="d-flex justify-content-center">
              <form method = "get" class="form-inline my-2 my-lg-4" action = "{{route('clients.search')}}">
            <input class="form-control mr-sm-2" type="search" name="search" placeholder="type clinet id" required size="9" minlength="9" maxlength="9">
            <span class="input-group-prepand">
                <button type="submit" class="btn btn-outline-success my-2 my-sm-0">search</button>
            </span>
            </div>
</form>
</div>
</div>
        </div>  
        </div>  
        </div>  

@endsection
