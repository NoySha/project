@extends('layouts.app')

@section('title', 'Creatc e Client')
@section('page title', 'Create Client')

@section('content')


@if(Session::has('exist'))
<div class = 'alert alert-danger'>
    {{Session::get('exist')}}
</div>
@endif

@if(Session::has('existmailclient'))
<div class = 'alert alert-danger'>
    {{Session::get('existmailclient')}}
</div>
@endif

@if(Session::has('id_error'))
<div class = 'alert alert-danger'>
    {{Session::get('id_error')}}
    </div>
@endif
 
@if(Session::has('phone_error'))
<div class = 'alert alert-danger'>
    {{Session::get('phone_error')}}
    </div>
@endif



<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-8">
 <div class="card">
 <div class="card-body">
        <form method = "post" action = "{{action('ClientsController@store')}}"> <!--טופס שמאפשר להוסיף מועמד חדש-->
        <!-- הראוט מסוג פוסט האקשין זו הכתובת אליה הנתונים נשלחים-->
        @csrf 
        <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('Id') }}</label>

                            <div class="col-md-6">

                                <input id="id" type="text" class="form-control @error('id') is-invalid @enderror" name="id" required autocomplete="name" autofocus  required size="9" minlength="9" maxlength="9">

                                @error('id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
        
    <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


    <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>
                            <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" required autocomplete="phone"  required size="9" minlength="9" maxlength="9">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 

    <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
    <div class="form-group row">
    <label for = "HMO" class="col-md-4 col-form-label text-md-right">HMO</label>
    <div class="col-md-6">
        <select class="form-control" name="HMO_id" required>
        <option value="" selected disabled>Please select</option>
        @foreach($hmos as $hmo)
            <option value="{{$hmo->HMO_id}}">{{$hmo->name}}</option>
        @endforeach
        </select>
        </div>
    </div>
    <div class="form-group row">
        <label for = "psychologist" class="col-md-4 col-form-label text-md-right">psychologist</label>
        <div class="col-md-6">
        <select class="form-control" name="user_id" required>
        <option value="" selected disabled>Please select</option>
        @foreach($users as $user)
        @if($user->role_id==3)
            <option value="{{$user->id}}">{{$user->name}}</option>
            @endif
        @endforeach
        </select>
    </div>
    </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                Crete Client 
                </button>
            </div>
        </div>

</form>  
        
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection
