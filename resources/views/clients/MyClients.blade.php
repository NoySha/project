@extends('layouts.app')

@section('title', 'My Clients')
@section('page title', 'My Clients')

@section('content')


<!--<h1 class="text-hide">Custom heading</h1>-->
<div class="card">
<div class="card-body">
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-8">
 <div class="card-body">
<div class="d-flex justify-content-center">
<form method = "get" class="form-inline my-2 my-lg-0" action = "{{route('clients.search')}}">
            <input class="form-control mr-sm-2" required size="9" minlength="9" maxlength="9" type="search" name="search" placeholder="type clinet id">
            <span class="input-group-prepand">
                <button type="submit" class="btn btn-outline-success my-2 my-sm-0">search</button>
            </span>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="card">


<table class="table table-sm">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Status</th>
        <th>HMO</th>
        <th></th>


    </tr>
    <!-- the table data -->
    @foreach (App\Client::myClients($user->id) as $client)
            <td>{{$client->id}}</td>
            <td>{{$client->name}}</td>
            <td>{{$client->phone}}</td>    
            <td>{{$client->email}}</td>
            <td>
                    {{$client->status->status_name}}
            </td>
            <td>
                    {{$client->hmos->name}}
            </td>
            <td>
            <a href = "{{route('clients.show',$client->id)}}"><button type="submit" class="btn btn-sm btn-success">show</button></a>
            </td>
        </tr>
    @endforeach
</table>
</div>
@endsection

