@extends('layouts.app')

@section('title', 'List of Clients')
@section('page title', 'List of Clients')

@section('content')

@if(Session::has('nomeetings'))
<div class = 'alert alert-danger'>
    {{Session::get('nomeetings')}}
</div>
@endif

<div class="row justify-content-center">
    <h1>
    Is It The Right Client?
    </h1>
</div>


<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH8AAAB/CAMAAADxY+0hAAAAbFBMVEX///82NjYzMzMsLCwvLy8AAAAmJiYpKSkjIyMeHh4aGhr6+vrw8PAYGBjS0tIVFRWfn59FRUW5ublYWFjg4OBfX1+ysrJNTU1SUlLY2Nh3d3eZmZmLi4vp6ek7OzusrKxqamrExMQNDQ2AgIDnsGuzAAAGOElEQVRoge1b2Y6jOhCNbcIeCFsg7JD//8cLAYIhGBc9U7SuNOdhNGo1HGPXesp9ufzDP/wAxW8vIPAL6/fYwyL0yjhJgiCI8uppn81vZ3VNByia4ZZpcyK5VdW6SymZQZW2fJzEbudXXSHfMIPwDHrHUzfIezDi49MXdOvbx1MwKnT+iAnpO1yxzTC87tET4iHHhHz38wkxHFT6UNunJ5SliDvQKFTC37lhhkYfGnL6zgbRNiAWux4HHSsQOi6EnrAciV9m+yOUCIk/gpx+5wIljgFYMHpCVCQDTIDfjxUDgfuPdv4vkPsRJUXiB9o/mv/dZcF/gHZH4n/oIH60+NcYMH6sGiS8gfjNJxJ/0YL4W7QyWFJ7jbhi0V9g34/HH0MCIPWQ2G1Sgr6/pDjdqA8rPwhxcdqgChZ+uhocpwt6wMJPx48TAB2Y+XcBAKcHKWDu3zkgjjZkwdJvl4CR6p8aWP/UOPS/nv8dYP5Da4FhAcjFor+8RMrPYvtfaPyFKj8B84aoglnSFtRtUKVQy9v3QUqQ9R9JD4LWe0zw95OQji0AWvtJAE97mVDvHQBa6zmj2YsB+gkCcLnnASdMRO5iCzTwpL8ZtjAEUO+UMUwqskC0xL+EUIah+NbfIxDyJ2cM5CyhDEbjM85/z/7OGIqG4gCgnDEAe4oDMMNSPng44jYQefgyoDGF/LcTwv/FF/OfMP7r4r94/9Uz4n++w4+lvH5QFXtdUNd4W3fEGBhGtPvXF23AW3j1EiwntB6KlvT/aTbHcNR46661qlUoW9DU7ZRhnxsLoKPzp4zcouYvr6B41O2t42RjeV+QtRahaGPsfXVNOjXb+vG3coH1vMe6OgTdif9SJMsyVIsnunwQCRTV8O7PP94G28ljZd7tD//Fjngr1IJP5s0+C6O6EufOH6Rkq3nFrcof9cx/sep5BzSu8Mj4jaFa6738H+2CXdVGy1Z2xvj2Lp164VvNMeSr/oAy91ZXx4zBcjJv847Lgv+SDQtoF1Ev3xCJFPVaZg5sG2wnS0umbtcYS/5L1aei21Juem2LVFRnZZrJxYEqbvX1rgv5LxX7UrtSoUpImd7GkgxZu7v6wrq/fLGvjl/M/16DGe0dQyARmGjC/bJ1yftUZCwXsNsfd9B2xhM7rd3Iv3g4GyqRZdcXyIa0urBIsKX65oL/o8cbvAlIBKIOpsgZK3FlNYG7WsBNI3nB35O+Q7gB8vk6JfPauRSkzJKnvSsPDC8Jtk1QereoZ5qLC9ubFqCU86pCgEZubFcoFWC+wgssYTwsmHlc27Mrz0wv2T4AqeWSVQAqov4JmvBd11b4XYPGW/SF/MEOLf/IeyDAFl+jgUaUW32isKxcwOCG6+HgL/y8+QmS6DcHZClousE4dX1MNazmth80I9oUaoDTpXm49Jye0D9tpyX3vjfMb/oQON2bh6ufTDtvgAMc0W2MaCrYcInQiSycPe3T9wulsRW0bw/cz5scjNHcuNsok+pdQN/xrRNbwM/vyMZyi+uDp75Xmj8/aNchWHavdcZ0vYsrNMeoZIHuJwz8awMAD9e7A/DXBzb6UwN/x5dSALUc8gmf0fwEJeufSN9RL+lt4N26N4ZOk78K+/bnEHz631J1AZksThiOm4+076wAST0fmMsUALxaOqKf8iwGQe8fHHrF6o5IduT739Zj8/x9CtrRpTbAllPaA6bTo7SX8frmHHG+HjTg6Q8+TG7VUobssjL4ds7IvzDAHU15++lkWS50TRjwcuj8Ct4AnSOm28NsFpch2N054Hxv8IXMxb8eXL0SZHyuYbms7VqDXvmmxaoCHZq7Riz7jIP2w4ykWgYgy0mZevAbfghF1eotMcL2o6t+8BwOg+rX6CFUIayqvpmAv3H4IbditHUlkUBsv/ZMUBV/kFwzy/phQ0SgwslLetQed8F0pcybIyJY6Ke6q4qVIDAoU10j9X8iioePPL66+o/tgSqqe/Ve1Z9M5azmnibdFzB6ZBWUMsNVY4jgBoBdOFUaxEQzdU2yjI5YN1USB2nlFH93GmuFTnN/BaWuqz00pimKQhVKFcY0TX3/VC+TNPOdEHUObD8d/1FleZ7WUf/nn3Wa59n98XCev/43sf/wf8N/EnFZVHTj6OcAAAAASUVORK5CYII=" alt="client" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4>{{$client->name}}</h4>

                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <div class="row justify-content-center">
                        <form method = "get" action = "{{route('meetings.showclientmeet',$client->id)}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-info">show meetings</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                    </li>
                    


                    @if(Gate::allows('Meetings'))
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <div class="row justify-content-center">
                        <form method = "get" action = "{{route('meetings.create',$client->id)}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-success">add meeting</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                    </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <div class="row justify-content-center">
                        <form method = "get" action = "{{route('clients.edit',$client->id)}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-primary">edit client</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                  </li>
                  @endif  

                  @if(Gate::allows('ClientsList'))

                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <div class="row justify-content-center">
                      <form method = "get" action = "{{route('clients.index')}}">
                          <div class="input-group">
                                  <span class="input-group-prepand">
                                      <button type="submit" class="btn btn-warning">go back</button>
                                  </span>
                          </div>
                      </form>
                  </div>
                  </li>
                  @endif

                  @if(Gate::allows('ShowMyClients'))
                    <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <div class="row justify-content-center">
                        <form method = "get" action = "{{route('clients.medicalHistory',$client->id)}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-success">Medical History</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                    </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <div class="row justify-content-center">
                        <form method = "get" action = "{{route('viewfile',$client->id)}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-primary">Medical Files</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                  </li>



                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                  <div class="row justify-content-center">
                        <form method = "get" action = "{{route('Clients.MyClients')}}">
                            <div class="input-group">
                                    <span class="input-group-prepand">
                                        <button type="submit" class="btn btn-warning">go back</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                   </li>
                   @endif                  



                </ul>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9">
                      {{$client->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">ID</h6>
                    </div>
                    <div class="col-sm-9">
                      {{$client->id}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->email}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->phone}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">HMO</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->hmos->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Psychologist</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->user3->name}}
                    </div>
                  </div>
                </div>
              </div>
      
            </div>
          </div>
        </div>
    </div>



@endsection

