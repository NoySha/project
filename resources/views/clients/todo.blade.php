@extends('layouts.app')

@section('title', 'To Do')
@section('page title', 'To Do')
@section('content')
     <!-- TO DO List -->
     <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  To Do List
                </h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body">

                <ul class="todo-list" data-widget="todo-list">
                @foreach($clients as $client)

                  <li>
                    <!-- drag handle -->
                    
                    <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>

                      <a href="{{route('clients.complete',$client->id)}}"><i class="fas fa-check" aria-hidden="true"></i></a>

                    <!-- add meeting -->
                    @if(Gate::allows('Meetings'))
                       <a href="{{route('meetings.create',$client->id)}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    @endif  

                    <!-- todo text -->
                    
                    <span class="text">Schedule a meeting for: {{$client->name}}</span>
                    <!-- Emphasis label -->
                    <small class="badge badge-danger"><i class="far fa-clock"></i> today</small>
                    <!-- General tools such as edit or delete-->
   
                  </li>
             @endforeach
                </ul>
              </div>
</div>

@endsection