@extends('layouts.app')

@section('title', 'Medical History')
@section('page title', 'Medical History')

@section('content')


<!--<h1 class="text-hide">Custom heading</h1>-->
<div class="row justify-content-center">
    <div class="card">
              <div class="card-header">
                  Select Treatment Status
              </div>
            <div class="dropdown">
                        @if (isset($client->status_id))
                            @if($client->status_id == '1')
                            <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @elseif($client->status_id == '2')
                            <button class="btn btn-warning" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @elseif($client->status_id == '3')
                            <button class="btn btn-info" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @elseif($client->status_id == '4')
                            <button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @endif
                           {{$client->status->status_name}}
                        @else
                            Define Status
                        @endif
                    </button>
                    <div class="dropdown-menu"  aria-labelledby="dropdownMenuButton">
                        @foreach($statuses as $status)
                        <a class="dropdown-item" href="{{route('clients.changestatus',[$client->id,$status->status_id])}}">{{$status->status_name}}</a>
                        @endforeach
                    </div>
            </div>
    </div>
</div>

<div class="row justify-content-center">
    <div class="card">
<table class="table table-striped">

    
        <th>Client</th>
        <th>Date</th>
        <th>Hour</th>
        <th colspan="4">
        <div style="max-width: 500px; width: 300px;">
        Summary</div></th>
        <th colspan="4"> 
        <div style="max-width: 500px; width: 300px;">
        Add/Change Summary
        </div>
        </th>
        <th></th>
    </tr>
    <!-- the table data -->
    @foreach (App\Meeting::medicalHistory($client->id) as $meeting)
    <tr>
            <td>{{$meeting->clients2->name}}</td> 
            <td>{{$meeting->date}}</td>
            <td>{{$meeting->available->start_hour}}</td>

            <td colspan="4">
            <textarea name="summary" READONLY class="form-control"  cols="15" rows="3" > {{$meeting->summary}}</textarea>
            </td>

            <td colspan="4">
            <form method = "get" action = "{{route('meetings.addsummary',$meeting->id)}}">
            <div class="form-group">
            <textarea name="summary" class="form-control"  cols="15" rows="3" > {{$meeting->summary}}</textarea>
            </div>    
            </td>

            <td>
            <button type="submit" class="btn btn-success">Submit</button>
            </form>
            </td>
        </tr>
        
    @endforeach
    </table>
</div>
</div>
@endsection

