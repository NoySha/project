@extends('layouts.app')

@section('title', 'List Of Users')
@section('page title', 'List Of Users')

@section('content')

<h1 class="text-hide">Custom heading</h1>
<div class="card">


<table class="table table-sm">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        @if (auth()->check())
            @if (auth()->user()->isAdmin())
            <th>change role</th>
            @endif
        @endif    
        @can ('edit-user')
            <th></th>
        @endcan
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>

            <td>
                {{$user->roles->name}} 
            </td>
            @if (auth()->check())
                @if (auth()->user()->isAdmin())
                <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($user->role_id))
                          {{$user->roles->name}}
                        @else
                          Change role  
                        @endif
                     </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($roles as $role)
                      <a class="dropdown-item" href="{{route('users.changerole',[$user->id,$role->id])}}">{{$role->name}}</a>
                    @endforeach
                    </div>
                </div>                
            </td>  
                @endif
            @endif
            @can ('edit-user')
            <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                            <a class="dropdown-item" href="{{route('users.edit',$user->id)}}">{{ __('Edit Email') }}</a>
                                                            <a class="dropdown-item" href="{{route('users.editpass',$user->id)}}">{{ __('Change Password') }}</a>
                                                </div>
                                            </div>
                                        </td>
            @endcan

        </tr>
    @endforeach
</table>

</div>



<div class="container">
<div class="row justify-content-center">
{{$users->links()}}
</div>
</div>

@endsection

