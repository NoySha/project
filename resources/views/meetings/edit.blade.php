@extends('layouts.app')

@section('title', 'Edit meeting')
@section('page title', 'Edit meeting')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit meeting</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('meetings.update', $meeting->id)}}">
                        @csrf
                            <div class="form-group">
                                <label for = "name">meeting date</label>
                                <input type = "date" class="form-control" name = "date" value = {{$meeting->date}}> <!--מכיר את השם מהקונטרולר-->
                            </div>     
                            <div class="form-group">
                                <label for = "email">meeting hour</label>
                                <input type = "string" class="form-control" name = "hour" value = {{$meeting->hour}}>
                            </div> 

                            
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                    update meeting 
                                    </button>
                                </div>
                            </div>                      
                        </form>    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
                    