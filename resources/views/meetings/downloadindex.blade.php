
<html>

<head>
    <style>
        #emp{
            border-collapse:collapse;
            width:100%;
        }
        #emp td,#emp th{
            border: 1px solid #ddd;
            padding: 8px;
            text-align:center;
        }
    </style>
</head>

<body>

<h1>Meetings list</h1>
<table id="emp">
    <tr>
        <th>Date</th>
        <th>Hour</th>
        <th>Psychologist</th>
        <th>Client</th>


    </tr>
    <!-- the table data -->
    @foreach($meetings as $meeting)
    <tr>
            <td>{{$meeting->date}}</td>
            <td>{{$meeting->available->start_hour}}</td>
            <td>
            @if(isset($meeting->user_id))
                {{$meeting->user->name}}
            @endif
            <td>
            @if (isset($meeting->client_id))
                {{$meeting->clients2->name}}
            @endif
            </td>      

        </tr>
    @endforeach

    
</table>

</body>
</html>
