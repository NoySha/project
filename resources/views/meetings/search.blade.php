@extends('layouts.app')

@section('title', 'find meeting')
@section('page title', 'find meeting')

@section('content')

@if(Session::has('nomeetingsdate'))
<div class = 'alert alert-danger'>
    {{Session::get('nomeetingsdate')}}
</div>
@endif

@if(Session::has('nomeetingsuser'))
<div class = 'alert alert-danger'>
    {{Session::get('nomeetingsuser')}}
</div>
@endif

@if(Session::has('nomeeting'))
<div class = 'alert alert-danger'>
    {{Session::get('nomeeting')}}
</div>
@endif

@if(Session::has('notselected'))
<div class = 'alert alert-danger'>
    {{Session::get('notselected')}}
</div>
@endif


 <div class="container">
 <div class="card">

 <div class="row justify-content-center">
 
 <div class="col-md-12">
 <div class="card-header">
 <div class="row justify-content-center">

    <h1>please select filter<h1>
    </div>
    <div class="row justify-content-center">

    <h2>you may filter by date / psychologist / both </h2>
    </div>
    </div>

    <div class="card-body">
<table>
<tr>
    <form method = "get" action = "{{route('meetings.showselected')}}">

        <div class="form-group row">
                <label for = "date" class="col-md-4 col-form-label text-md-right">Meeting date</label>
                <div class="col-md-6">
                    <input type = "date" name = "date" class="form-control">
                </div>
        </div>     
</tr>
<tr>

        <div class="form-group row">
            <label for = "psychologist" class="col-md-4 col-form-label text-md-right">psychologist</label>
            <div class="col-md-6">
                <select class="form-control" name="user_id">
                <option disabled selected>Please Choose</option>
                @foreach($users as $user)
                    @if($user->role_id==3)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endif
                @endforeach
                </select>
            </div>
            </tr>
            <tr>
            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
        <span class="input-group-prepand">
                    <button type="submit" class="btn btn-primary">search</button>
        </span>
            
    </div>
    </div>

</tr>
</form>

</table>

</div>  
</div>  
</div>  
</div>
</div>
@endsection
