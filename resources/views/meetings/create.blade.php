@extends('layouts.app')

@section('title', 'Create Meeting')
@section('page title', 'Create Meeting')

<head>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

@section('content')



@if(Session::has('noavailable'))
<div class = 'alert alert-danger'>
    {{Session::get('noavailable')}}
</div>
@endif

<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
            <div class="col-md-5 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">



                    <div class="mt-3">
                      <h4>find availble meetings</h4>

                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
              <form method = "post" action="{{route('meetings.find')}}"> <!--טופס שמאפשר להוסיף מועמד חדש-->
        <!-- הראוט מסוג פוסט האקשין זו הכתובת אליה הנתונים נשלחים-->
        @csrf 


        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "date" class="col-md-5 col-form-label text-md-right">psychologist</label>
            <div class="col-md-6">
               <input type="text" READONLY value="{{$client->user3->name}}" class="form-control-plaintext" >
            </div>
          </div> 
        </div>



        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "date" class="col-md-5 col-form-label text-md-right">client</label>
            <div class="col-md-6">
               <input type="text" READONLY value="{{$client->name}}" class="form-control-plaintext" >
            </div>
          </div> 
        </div>


 <!--  
     <div class="form-group row">
            <label for = "date" class="col-md-4 col-form-label text-md-right">Meeting date</label>
            <div class="col-md-6">
            <input type = "date" name = "date" class="form-control">
            </div>
        </div>     
        <div class="form-group row">
            <div class="col-md-6">
        </div>
        </div> 
       -->     

        <div class="form-group row">
          <label for = "date" class="col-md-4 col-form-label text-md-right">Meeting date</label>
          <div class="col-md-6">

          <input type="text" name="date" class="form-control datepicker" autocomplete="off">
      </div>
      </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                search available hours 
                </button>
            </div>
        </div>

        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "date" class="col-md-5 col-form-label text-md-right"></label>
            <div class="col-md-6">
               <input type="hidden" READONLY name="client_id" value="{{$client->id}}" class="form-check-input" >
            </div>
          </div> 
        </div>
        


   
  



        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "date" class="col-md-5 col-form-label text-md-right"></label>
            <div class="col-md-6">
               <input type="hidden" READONLY name="user_id" value="{{$client->user3->id}}" class="form-check-input" >
            </div>
          </div> 
        </div>
        </form>   


                
              </div>
            </div>
            <div class="col-md-7">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-7">
                      {{$client->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">ID</h6>
                    </div>
                    <div class="col-sm-7">
                      {{$client->id}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-7">
                    {{$client->email}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-7">
                    {{$client->phone}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">HMO</h6>
                    </div>
                    <div class="col-sm-7">
                    {{$client->hmos->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Psychologist</h6>
                    </div>
                    <div class="col-sm-7">
                    {{$client->user3->name}}
                    </div>
                  </div>
                </div>
              </div>
      
            </div>
          </div>
        </div>
    </div>



    <script type="text/javascript">
    var disableDates =<?php echo json_encode($avails)?>;
      
    $('.datepicker').datepicker({
        format: 'yyyy/mm/dd',
        beforeShowDay: function(date){
            //dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
            ymd = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            if(disableDates.indexOf(ymd) == -1){
                return false;
            }
            else{
                return true;
            }
        }
    });
</script>

@endsection
