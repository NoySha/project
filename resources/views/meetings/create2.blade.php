@extends('layouts.app')

@section('title', 'Create Meeting')
@section('page title', 'Create Meeting')

@section('content')



<div class="container">
    <div class="main-body">
    
          <div class="row gutters-sm">
          <div class="col-md-5 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">



                    <div class="mt-3">
                      <h4>find availble meetings</h4>

                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
              <form method = "post" action="{{route('meetings.store')}}"> <!--טופס שמאפשר להוסיף מועמד חדש-->
        <!-- הראוט מסוג פוסט האקשין זו הכתובת אליה הנתונים נשלחים-->
        @csrf 

        


        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "name" class="col-md-5 col-form-label text-md-right">client</label>
            <div class="col-md-6">
               <input type="text" READONLY value="{{$client->name}}" class="form-control-plaintext">
            </div>
          </div> 
        </div>

        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "user" class="col-md-5 col-form-label text-md-right">psychologist</label>
            <div class="col-md-6">
               <input type="text" READONLY value="{{$client->user3->name}}" class="form-control-plaintext">
            </div>
          </div> 
        </div>



        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "date" class="col-md-5 col-form-label text-md-right">date</label>
            <div class="col-md-6">
               <input type="text" READONLY name="date" value="{{$date}}" class="form-control-plaintext">
            </div>
          </div> 
        </div>

        <div class="form-group row">
        <label for = "hour" class="col-md-4 col-form-label text-md-right">hour</label>
        <div class="col-md-6">

            <select class="form-control" name="hour">
  
            @foreach($hours as $hour)
]                        <option value="{{$hour->id}}">{{$hour->start_hour}}</option>
                      
            @endforeach
            </select>
        </div>
        </div>


    

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-success">
                create meeting 
                </button>
            </div>
        </div>

        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "user_id" class="col-md-5 col-form-label text-md-right"></label>
            <div class="col-md-6">
               <input type="hidden" READONLY name="user_id" value="{{$client->user3->id}}" class="form-control-plaintext">
            </div>
          </div> 
        </div>

        
        <div class="col-md-10 mb-4">
          <div class="form-group row">
            <label for = "client_id" class="col-md-5 col-form-label text-md-right"></label>
            <div class="col-md-6">
               <input type="hidden" READONLY name="client_id" value="{{$client->id}}" class="form-control-plaintext">
            </div>
          </div> 
        </div>

        </form>   
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <a href="{{route('meetings.create',$client->id)}}"><button type="submit" class="btn btn-warning">
                choose other date
                </button></a>
            </div>
        </div>

        <div class="col-md-10 mb-4">
          <div class="form-group row">
          
            <label for = "id" class="col-md-5 col-form-label text-md-right"></label>
            <div class="col-md-6">
               <input type="hidden" READONLY name="client_id" value="{{$client->id}}"class="form-control-plaintext">
            </div>
          </div> 
        </div>

        

              </div>
            </div>
            <div class="col-md-7">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9">
                      {{$client->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">ID</h6>
                    </div>
                    <div class="col-sm-9">
                      {{$client->id}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->email}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->phone}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">HMO</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->hmos->name}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Psychologist</h6>
                    </div>
                    <div class="col-sm-9">
                    {{$client->user3->name}}
                    </div>
                  </div>
                </div>
              </div>
      
            </div>
          </div>
        </div>
    </div>






@endsection
