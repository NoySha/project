@extends('layouts.app')

@section('title', 'My Meetings')
@section('page title', 'My Meetings')

@section('content')

@if(Session::has('nomeetingsuser'))
<div class = 'alert alert-danger'>
    {{Session::get('nomeetingsuser')}}
</div>
@endif

@if(Session::has('notselectedpsy'))
<div class = 'alert alert-danger'>
    {{Session::get('notselectedpsy')}}
</div>
@endif


<div class="card">
<div class="card-body">
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-8">
 <div class="card-body">
<div class="d-flex justify-content-center">
<form method = "get" class="form-inline my-2 my-lg-0" action = "{{route('meetings.showselectedpsy')}}">
<input type = "date" name = "date" class="form-control">
            <span class="input-group-prepand">
                <button type="submit" class="btn btn-outline-success my-2 my-sm-0">search meeting</button>
            </span>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="card">

<table class="table table-sm">
    <tr>
        <th>Date</th>
        <th>Hour</th>
        <th>Client</th>

    </tr>
    <!-- the table data -->
    @foreach ($meetings as $meeting)
            <td>{{$meeting->date}}</td>
            <td>{{$meeting->available->start_hour}}</td>
            <td>
                           {{$meeting->clients2->name}}
            </td>      
        </tr>
    @endforeach
</table>
</div>
@endsection

