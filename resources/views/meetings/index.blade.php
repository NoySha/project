@extends('layouts.app')

@section('title', 'List of Meetings')
@section('page title', 'List Of Meetings')

@section('content')



<div class="card">
    <div class="row gutters-sm">
    <div class="row justify-content-center">
 <div class="col-md-9">
@if(Gate::allows('Meetings'))
<form method = "get" action = "{{route('clients.search1')}}">
       <div class="input-group">
            <span class="input-group-prepand">
                <button type="submit" class="btn btn-sm btn-success">add meeting</button>
            </span>
       </div>
</form>
@endif
</div>
</div>
<div class="row justify-content-center">
 <div class="col-md-9">
<form method = "get" action = "{{route('meetings.search')}}">
       <div class="input-group">
            <span class="input-group-prepand">
                <button type="submit" class="btn btn-sm btn-info">search meeting</button>
            </span>
       </div>
</form>
</div>
</div>

<table class="table table-sm">
    <tr>
        <th>Date</th>
        <th>Hour</th>
        <th>Psychologist</th>
        <th>Client</th>


    </tr>
    <!-- the table data -->
    @foreach($meetings as $meeting)
            <td>{{$meeting->date}}</td>
            <td>{{$meeting->available->start_hour}}</td>
            <td>
            @if(isset($meeting->user_id))
                {{$meeting->user->name}}
            @endif
            <td>
            @if (isset($meeting->client_id))
                {{$meeting->clients2->name}}
            @endif
            </td>      

     

            <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                   
                                                        <form action="{{route('meetings.delete',$meeting->id)}}" method="get">
                                                            @csrf
                                                            @method('delete')
                                                            
                                                            <button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this task?") }}') ? this.parentElement.submit() : ''">
                                                                {{ __('Delete') }}
                                                            </button>
                                                        </form>    
                                                    
                                                       
                                                </div>
                                            </div>
                                        </td>

        </tr>
    @endforeach

    
</table>
</div>


</div>

<div class="container">
<div class="row justify-content-center">
{{$meetings->links()}}
</div>
</div>
@endsection

