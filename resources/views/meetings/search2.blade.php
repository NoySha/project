@extends('layouts.app')

@section('title', 'find meeting')
@section('page title', 'find meeting')

@section('content')



<div class="container">
 <div class="card">

 <div class="row justify-content-center">
 
 <div class="col-md-12">
 <div class="card-header">
 <div class="row justify-content-center">

    <h1>please select<h1>
    </div>
    </div>
    <div class="card-body">

    <div class="row justify-content-center">
    <form method = "get" action = "{{route('meetings.showselected2')}}">
        <input type="hidden" READONLY name="date" value="{{$date}}" class="form-control-plaintext">
        <input type="hidden" READONLY name="user_id" value="{{$user_id}}" class="form-control-plaintext">
                <span class="input-group-prepand">
                    <button type="submit" class="btn btn-primary">show list</button>
                </span>

    </form>
        </div>


        <div class="row justify-content-center">
    <form method = "get" action = "{{route('downloadPDF')}}">
        <input type="hidden" READONLY name="user_id" value="{{$user_id}}" class="form-control-plaintext">
        <input type="hidden" READONLY name="date" value="{{$date}}" class="form-control-plaintext">
        <span class="input-group-prepand">
            <button type="submit" class="btn btn-info">export as PDF</button>
        </span>
        </div>
    </form>


</div>  
</div>  
</div>  
</div>  
</div>  


@endsection
