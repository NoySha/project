<div class="header p-3 mb-1 bg-gradient-warning text-dark">
        <div class="header-body text-center mb-7">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">@yield('page title')</h1>
                </div>
            </div>
        </div>
    </div>
