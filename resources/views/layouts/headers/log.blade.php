

<div class="header p-3 mb-1 bg-gradient-warning text-dark">
<nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item dropdown">

      @if (Route::has('login'))
         @auth
        <a class="nav-link" href="{{ url('/home') }}">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
        <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
        </svg>
        
        <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-home"></span> Home
        </button>
        </a>
        @else
            <a class="nav-link" href="{{ route('login') }}">
            <i class="fas fa-key"></i>
            <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-home"></span> LOGIN
        </button>
        </a>
        @endauth
        @endif
    </ul>
  </nav>

  <!-- /.navbar -->
        <div class="header-body text-center mb-7">
            <div class="row justify-content-center">
            
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">{{ __('Welcome') }}</h1>
                </div>
            </div>
        </div>
    </div>
        <svg x="100" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
</div>
