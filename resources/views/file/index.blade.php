@extends('layouts.app')

@section('title', 'file')
@section('page title', 'file')

@section('content')

<div class="container">
	@if(session('success'))
		<div class="alert alert-success">
			<strong>{{ session('success') }}</strong>
		</div>
	@endif
	<p>
		<a href="{{ route('formfile',$client->id) }}" class="btn btn-success">Upload File</a>
	</p>
	<div class="row">
		@foreach($files as $file)
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<strong class="card-title">{{ $file->title }}</strong>
					<p class="card-text">{{ $file->created_at->diffForHumans() }}</p>
					<form action="{{ route('deletefile', $file->id) }}" method="get">
						@csrf
						@method('DELETE')
						<a href="{{ route('downloadfile', $file->id) }}" class="btn btn-primary">Download</a>
						<button type="submit" class="btn btn-danger">Delete</button>
						<h5 class="box-title">Please open with "chrome"</h5>

						
					</form>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>


@endsection