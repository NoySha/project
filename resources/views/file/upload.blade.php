@extends('layouts.app')

@section('title', 'file')
@section('page title', 'file')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload File</div>
                    <div class="card-body">

<form action="{{route('uploadfile',$client->id)}}" method="post" enctype="multipart/form-data">
@csrf

<div class="file-field">
    <a class="btn-floating peach-gradient mt-0 float-left">
      <i class="fas fa-paperclip" aria-hidden="true"></i>
      <input type="file" name="file">
    </a>
  </div>




<div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">title</label>
                            <div class="col-md-6">
                                <select class="form-control" name="type">                                                                         
                                   @foreach ($types as $type)
                                   <option value="{{$type}}"> 
                                        {{ $type }} 
                                    </option>
                                   @endforeach    
                                 </select>
                            </div>
    </div>

<button type="submit" class="btn btn-warning">Upload</button>
<a href="{{route('viewfile',$client->id)}}" class="btn btn-success">Back</a>
</form>

</div>
</div>
</div>
</div>
</div>


@endsection