@extends('layouts.app')

@section('title', 'Dashboard')

@section('page title', 'CLINIC')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="row justify-content-center">

                <div class="card-header">
                <h1 class="text-warning">HELLO {{Auth::user()->name}}</h1>
                </div>
               
          </div>

          <div class="card-body">
          
              @if (session('status'))
                  <div class="alert alert-success" role="alert">
                      {{ session('status') }}
                  </div>
              @endif


            <div class="row justify-content-center">

            <h2 class="text-warning">have a nice day!</h2>
            </div>

                </div>
            </div>
    </div>
</div>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3-week" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
  <path fill-rule="evenodd" d="M12 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm2-3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-5 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
</svg></span>

              <div class="info-box-content">
                <span class="info-box-text">Today Meetings</span>
                <span class="info-box-number">
                @if(Gate::allows('ShowMyClients'))
                <span class="info-box-number">{{DB::table('meetings')->where('user_id',Auth::id())->whereDay('date',now()->day)->count()}}</span>

                @else
                {{DB::table('meetings')->whereDay('date',now()->day)->count()}}
                @endif
                </span>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-calendar3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"/>
  <path fill-rule="evenodd" d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
</svg></span>

              <div class="info-box-content">
                <span class="info-box-text">This Month Meetings</span>
                @if(Gate::allows('ShowMyClients'))
                <span class="info-box-number">{{DB::table('meetings')->where('user_id',Auth::id())->whereMonth('date',now()->month)->count()}}
                @else
                <span class="info-box-number">{{DB::table('meetings')->whereMonth('date',now()->month)->count()}}
                @endif
              </span>
              </div>
            </div>
          </div>
</div>
<div class="row justify-content-center">
          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
              @if(Gate::allows('ShowMyClients'))
                <span class="info-box-text">Your Clients</span>
                <span class="info-box-number">{{DB::table('clients')->where('user_id',Auth::id())->count()}}
                @else
                <span class="info-box-text">Clients</span>
                <span class="info-box-number">{{DB::table('clients')->count()}}</span>
                @endif
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
</svg></span>

              <div class="info-box-content">
                <span class="info-box-text">Psychologist</span>
                <span class="info-box-number">{{DB::table('users')->where('role_id',3)->count()}}</span>
              </div>
            </div>
          </div>
        </div>
        <div>
       
</div>
</div>


@endsection
