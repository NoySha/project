@extends('layouts.app')
 
@section('title', 'Add Hours')
@section('page title', 'Add Hours')
 
@section('content')

@if(Session::has('upload'))
<div class = 'alert alert-success'>
    {{Session::get('upload')}}
</div>
@endif

@if(Session::has('notupload'))
<div class = 'alert alert-warning'>
    {{Session::get('notupload')}}
</div>
@endif



<div class="card">
    <div class="row gutters-sm">
    <div class="row justify-content-center">
 <div class="col-md-8">
          <h3 class="box-title">Select a file to import</h3>
          
        </div>


        <div class="box-body">
          <form action="{{route('bulk-import-user')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
 
            <div class="form-group">
              <input type="file" name="file" id="file" class="form-control" accept=".txt">
              <div class="HelpText error">{{$errors->first('file')}}</div>
            </div>
 
            <div class="form-group">
              <button class="btn btn-success">
                <i class="fa fa-upload"></i> Upload
              </button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
 

      </div>
    </div>
 
    <div class="col-sm-9">
      @if($errors = Session::get('errors'))
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="list-group">
              @foreach($errors as $error)
                <li class="list-group-item">{{$error}}</li>
              @endforeach
            </ul>
          </div>
          <!-- /.box-body -->
 
          <div class="box-footer">
 
          </div>
        </div>
      @endif
    </div>
  </div>


@endsection