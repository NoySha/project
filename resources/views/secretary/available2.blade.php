@extends('layouts.app')

@section('title', 'Add Hours')
@section('page title', 'Add Hours')

@section('content')

<div class="card">
    <div class="card-header">
        <div class="container">
            <div class="main-body">
                <div class="card shadow">
                    <div class="row gutters-sm">
                       
                        <label for = "date" class="col-md-4 col-form-label text-md-right">date</label>
                        <div class="col">
                            <input type="text" READONLY value="{{$date}}" class="form-control-plaintext">
                         </div>

                        <label for = "psychologist" class="col-md-0 col-form-label text-md-center">psychologist name</label>
                        <div class="col">
                            <input type="text" READONLY value="{{$user->name}}" class="form-control-plaintext">
                        </div>

                        <label for = "psychologist" class="col-md-14 col-form-label text-md-right"></label>
                        <div class="col">
                            <input type="hidden" READONLY name="user_id" value="{{$user->id}}" class="form-control-plaintext">
                        </div>

                        <div class="col mt--0">
                        <a href="{{url('available')}}"><button type="submit" class="btn-sm btn-info">
                            back
                        </button></a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>

 
<div class="card-body">
<div class="container-fluid mt--0">

        <div class="row float-left">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-30">
                                <h3 class="mb-0">Create Daily Planner</h3>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th scope="col">start hour</th>
                                    <th scope="col">status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($hours as $hour)
                            <form method="POST" action="{{route('available.store')}}">
                            @csrf  
                                <input type="hidden" READONLY name="user_id" value="{{$user->id}}" class="form-control-plaintext" >
                            
                                <input type="hidden" READONLY name="date" value="{{$date}}" class="form-control-plaintext" >
       
                                <input type="hidden" READONLY name="start_hour" value="{{$hour}}" class="form-control-plaintext">
                                
                                <td></td>
                                <td>{{$hour}}</td>
                                <td>
                                @if ((App\Available::addHours($hour,$date,$user->id)) == '0' )
                                <button type="submit" class="btn-sm btn-success">
                                    create
                                </button>
                                @else
                                schedule created
                                @endif
                            </form>       
                            </div>
                        </div>
                    </td>
                    <td></td>

                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>   
    </div>
<!-- end of code task table -->

<!-- topic table -->


<div class="container-fluid mt--0">
        <div class="row float-right">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-30">
                                <h3 class="mb-0">Delete Daily Planner</h3>
                            </div>
                        </div>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th scope="col">start hour</th>
                                    <th scope="col">status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($availables as $available)
                            <form method="get" action="{{route('available.deleteh',$available->id)}}">
                            @csrf  
                                <td></td>
                                <td>{{$available->start_hour}}</td>
                                <td>
                                @if($available->status == "available")
                                    <button type="submit" class="btn-sm btn-warning">
                                        Delete
                                    </button>
                                @else
                                Do not delete, because there is a meeting scheduled for this time
                                @endif
                                <td></td>

                            </form>       
                            </div>
                        </div>
                    </td>
                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>
             



@endsection