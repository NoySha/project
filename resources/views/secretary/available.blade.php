@extends('layouts.app')

@section('title', 'Add Hours')
@section('page title', 'Add Hours')

@section('content')
@if(Session::has('date'))
<div class = 'alert alert-danger'>
    {{Session::get('date')}}
</div>
@endif

@if(Session::has('user'))
<div class = 'alert alert-danger'>
    {{Session::get('user')}}
</div>
@endif

@if(Session::has('dateuser'))
<div class = 'alert alert-danger'>
    {{Session::get('dateuser')}}
</div>
@endif

     <div class="card">
              <div class="card-body">
              <form method="get" action="{{route('hours')}}">
                        @csrf     

              <div class="form-group row">
                    <label for = "date" class="col-md-4 col-form-label text-md-right">date</label>
                    <div class="col-md-6">
                    <input type = "date" name = "date" class="form-control">
                    </div>
              </div>     

              <div class="form-group row">
              <label for = "psychologist" class="col-md-4 col-form-label text-md-right">psychologist</label>
        <div class="col-md-6">

            <select class="form-control" name="user_id">
            <option disabled selected>Please Choose</option>

            @foreach($users as $user)
                        @if($user->role_id==3)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @endif
            @endforeach
            </select>
        </div>

</div>

    <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-success">
                set date & psychologist
                </button>
            </div>
        </div>
    </form>

    </div>
    </div>
@endsection
